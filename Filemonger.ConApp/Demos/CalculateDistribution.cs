﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Filemonger.ConApp.Demos
{
    [Description("Calculate Distribution")] 
    internal class CalculateDistribution : IDemo
    {
        private const int NumberOfFilesToAdd = 100000;

        public void Run()
        {
            //RunSet(new Filemonger(2, 4, 4), NumberOfFilesToAdd);
            //RunSet(new Filemonger(4, 2, 4), NumberOfFilesToAdd);
            //RunSet(new Filemonger(2, 2, 8), NumberOfFilesToAdd);

            const int prodSampleCount = 23230182;
            const int folderCount = 1 << 6;

            int mountPointCount = 1, topFolderCount = folderCount, subFolderCount = folderCount;

            double totFolders = mountPointCount*topFolderCount*subFolderCount;

            Console.WriteLine("File count           = {0}", prodSampleCount);
            Console.WriteLine("Total folder count   = {0}", totFolders);
            Console.WriteLine("Avg files per folder = {0}", prodSampleCount / totFolders);

            var filemoger = FilemongerFactory.Create(mountPointCount, topFolderCount, subFolderCount);

            Console.WriteLine(filemoger);
        }
    }
}