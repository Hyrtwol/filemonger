﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;

namespace Filemonger.ConApp.Demos
{
    // in this demo 4 mountpoint is available
    // but only 2 paths is defined
    // the mount point is then mapped so 75% of the files will end up in the first
    // and 25% in the last

    [Description("Fill repo with fake samples files")] 
    internal class FillWithSamples : IDemo
    {
        private const string SearchPath = @"C:\Windows\System32";
        private const int NumberOfFilesToAdd = 1000;

        public void Run()
        {
            const string rootPath = @"\\ix2-w-d-uni-01\90";
            //IO.DeleteDirectoryIfExists(rootPath);

            var mountPoints = new[]
                {
                    Path.Combine(rootPath, "00"),
                    Path.Combine(rootPath, "01"),
                    Path.Combine(rootPath, "02"),
                    Path.Combine(rootPath, "03")
                };

            var filemonger = FilemongerFactory.Create(4, 64, 64);

            foreach (var mountPoint in mountPoints)
            {
                filemonger.AddMountPoint(mountPoint);
            }


            for (int i = 0; i < mountPoints.Length; i++)
            {
                filemonger.BindMountPoint(i, i);
            }

            Console.WriteLine("Filemonger:");
            Console.WriteLine("  MountPointCount: {0}", filemonger.MountPointCount);
            Console.WriteLine("  Mask:            {0}", filemonger.Mask);


            //filemonger.

            //int filesAdded = 0;

            //var hashSet = new HashSet<string>();

            //foreach (var inputFileName in Directory.EnumerateFiles(SearchPath, "*.dll").Take(NumberOfFilesToAdd))
            //{
            //    var fileName = Path.GetFileName(inputFileName) + ".txt";

            //    if (hashSet.Contains(fileName)) throw new InvalidOperationException();
            //    hashSet.Add(fileName);

            //    var fileEntry = filemonger.GetFileEntry(fileName);
            //    CreateTextFile(fileEntry, inputFileName);
            //    filesAdded++;
            //}

            //Console.WriteLine("Files added: {0}", filesAdded);

            //int totalCount = filemonger.EnumerateFiles().Count();
            //Console.WriteLine("File count:  {0}", totalCount);

            //int count0 = Directory.EnumerateFiles(mountPoint0, "*.txt", SearchOption.AllDirectories).Count();
            //int count1 = Directory.EnumerateFiles(mountPoint1, "*.txt", SearchOption.AllDirectories).Count();

            //Console.WriteLine("{0}: {1} {2:F2}%", mountPoint0, count0, count0 * 100.0 / totalCount);
            //Console.WriteLine("{0}: {1} {2:F2}%", mountPoint1, count1, count1 * 100.0 / totalCount);
        }

        private static void CreateTextFile(FileEntry fileEntry, string orgFile)
        {
            using (var s = fileEntry.CreateText())
            {
                s.WriteLine(fileEntry.FullName);
                s.WriteLine(DateTime.Now);
                s.WriteLine(orgFile);
            }
        }
    }
}