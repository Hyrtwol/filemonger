﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Filemonger.ConApp.Demos
{
    [Description("Show Distribution")] 
    internal class ShowDistribution : IDemo
    {
        private const int NumberOfFilesToAdd = 100000;

        public void Run()
        {
            //RunSet(new Filemonger(2, 4, 4), NumberOfFilesToAdd);
            //RunSet(new Filemonger(4, 2, 4), NumberOfFilesToAdd);
            //RunSet(new Filemonger(2, 2, 8), NumberOfFilesToAdd);

            const int prodSampleCount = 23230182;
            const int folderCount = 64;

            int mountPointCount = 1, topFolderCount = folderCount, subFolderCount = folderCount;

            double totFolders = mountPointCount*topFolderCount * subFolderCount;

            Console.WriteLine("File count           = {0}", prodSampleCount);
            Console.WriteLine("Total folder count   = {0}", totFolders);
            Console.WriteLine("Avg files per folder = {0}", prodSampleCount / totFolders);

            RunSet(FilemongerFactory.Create(mountPointCount, topFolderCount, subFolderCount), prodSampleCount);
        }

        private static void RunSet(IFilemonger partitionMapper, int numberOfFilesToAdd)
        {
            Console.WriteLine(partitionMapper.ToString());

            var mask = partitionMapper.Mask;
            var folderFileCount = new Dictionary<int, int>();
            for (int i = 0; i <= mask.MountPoint; i++)
            {
                for (int j = 0; j <= mask.TopFolder; j++)
                {
                    for (int k = 0; k <= mask.SubFolder; k++)
                    {
                        folderFileCount[GetFolderId(i, j, k)] = 0;
                    }
                }
            }

            const string format = "\r{0,6:F1} % ";

            Console.WriteLine("Adding {0}", numberOfFilesToAdd);
            for (int i = 0; i < numberOfFilesToAdd; i++)
            {
                var fileName = MakeFileName(i);
                var muid = partitionMapper.GetFileIndex(fileName);
                int folderId = GetFolderId(muid);
                folderFileCount[folderId]++;
                if((i & 0x3ffff)==0)
                {
                    Console.Write(format, i * 100.0 / numberOfFilesToAdd);
                }
            }
            Console.WriteLine(format, 100.0);

            Console.WriteLine("Folders:");
            int idx = 0;
            var data = new int[folderFileCount.Count];
            foreach (var kv in folderFileCount)
            {
                data[idx] = kv.Value;
                Console.WriteLine("  {0,4} : {1:X6} = {2}", idx++, kv.Key, kv.Value);
            }

            var statistics = new Statistics(data);
            Console.WriteLine("Statistics:");
            Console.WriteLine("  Minimum   = {0,8:F3}", statistics.Minimum);
            Console.WriteLine("  Maximum   = {0,8:F3}", statistics.Maximum);
            Console.WriteLine("  Average   = {0,8:F3}", statistics.Average);
            Console.WriteLine("  Deviation = {0,8:F3}", statistics.SampleStandardDeviation);
        }

        private static string MakeFileName(int i)
        {
            return string.Format("{0:D8}.dat", i);
        }

        private static int GetFolderId(Hash hash)
        {
            return GetFolderId(hash.MountPoint, hash.TopFolder, hash.SubFolder);
        }

        private static int GetFolderId(int mountPoint, int topFolder, int subFolder)
        {
            return (mountPoint << 16) + (topFolder << 8) + subFolder;
        }
    }
}