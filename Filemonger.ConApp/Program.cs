﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Filemonger.ConApp
{
    static class Program
    {
        private static readonly List<Type> Demos = new List<Type>();

        static void Main(string[] args)
        {
            Console.WriteLine(" -= Filemonger =- ");
            Console.WriteLine();

            ListDemos();

            Console.WriteLine();
            Console.Write("> ");            

            Console.ReadLine().GetSelection().GetDemo().RunDemo();

            Console.Write("Done. ");
            Console.ReadKey();
        }

        private static void ListDemos()
        {
            foreach (var t in Assembly.GetExecutingAssembly()
                .GetTypes().Where(t => t.GetInterface("IDemo") != null))
            {
                foreach (var descriptionAttribute in t
                    .GetCustomAttributes(typeof (DescriptionAttribute), false)
                    .OfType<DescriptionAttribute>())
                {
                    Demos.Add(t);
                    Console.WriteLine("{0,3} - {1}", Demos.Count, descriptionAttribute.Description);
                }
            }
        }

        private static int GetSelection(this string input)
        {
            int selection;
            return !string.IsNullOrEmpty(input) && int.TryParse(input, out selection)
                       ? selection : 0;
        }

        private static IDemo GetDemo(this int selection)
        {
            if (selection < 1 || selection > Demos.Count) return null;
            var type = Demos[selection - 1];
            var obj = Activator.CreateInstance(type);
            return obj as IDemo;
        }

        private static void RunDemo(this IDemo demo)
        {
            if (demo == null) return;
            demo.Run();
        }
    }
}
