﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Filemonger.Demo
{
    public partial class MainForm : Form
    {
        //Pen pen = new Pen(Color.Green);
        //private void Form1_Paint(object sender, PaintEventArgs e)
        //{
        //    PaintGraph(e.Graphics);
        //}
        //private void PaintGraph(Graphics graphics)
        //{
        //    graphics.DrawLine(pen, 10, 10, 200, 200);
        //}

        private IFilemonger filemonger;

        public MainForm()
        {
            InitializeComponent();

            filemonger = null;
            UpdateUI();
        }

        private void UpdateUI()
        {
            treeView1.BeginUpdate();
            treeView1.Nodes.Clear();

            listView1.BeginUpdate();
            listView1.Items.Clear();

            if (filemonger == null)
            {
                label3.Text = "n/a";
                label2.Text = "n/a";
                label6.Text = "n/a";
                label7.Text = "n/a";
                label9.Text = "n/a";
                button2.Enabled = false;
                button3.Enabled = false;
            }
            else
            {
                label3.Text = filemonger.MountPointCount.ToString();
                label2.Text = filemonger.TopFolderCount.ToString();
                label6.Text = filemonger.SubFolderCount.ToString();
                label7.Text = filemonger.PotentialSubFolderCount.ToString();
                label9.Text = filemonger.Mask.ToString();
                foreach (var mountPoint in filemonger.GetMountPoints())
                {
                    var mountNode = treeView1.Nodes.Add(mountPoint);
                    foreach (var topFolder in filemonger.GetTopFolders())
                    {
                        var topNode = mountNode.Nodes.Add(topFolder);
                        foreach (var subFolder in filemonger.GetSubFolders())
                        {
                            topNode.Nodes.Add(subFolder);
                        }
                    }
                }

                foreach (var mountPoint in filemonger.GetBoundMountPoints())
                {
                    listView1.Items.Add(mountPoint ?? "n/a");
                }
                button2.Enabled = filemonger.AvailableMountPointCount < filemonger.MountPointCount;
                button3.Enabled = true;
            }

            listView1.EndUpdate();
            treeView1.EndUpdate();
        }

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    var f = new NewFilemonger();
        //    var res = f.ShowDialog();
        //    if (res != DialogResult.OK) return;

        //    filemonger = f.Create();
        //    //filemonger.AddMountPoint(Path.Combine(Directory.GetCurrentDirectory(), "Root1"));
        //    UpdateUI();
        //}

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            var listView = sender as ListView;
            if(listView == null) return;
            if (listView.SelectedIndices.Count != 1) return;
            var selected = listView.SelectedIndices[0];

            var f = new SelectMountPoint(filemonger.GetMountPoints());
            var res = f.ShowDialog();
            if (res != DialogResult.OK) return;

            filemonger.BindMountPoint(selected, f.SelectedIndex);
            UpdateUI();
        }

        private void AddMountPointClick(object sender, EventArgs e)
        {
            if(folderBrowserDialog1.ShowDialog() != DialogResult.OK) return;
            filemonger.AddMountPoint(folderBrowserDialog1.SelectedPath);
            UpdateUI();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new NewFilemonger();
            var res = f.ShowDialog();
            if (res != DialogResult.OK) return;

            filemonger = f.CreateFilemonger();
            //filemonger.AddMountPoint(Path.Combine(Directory.GetCurrentDirectory(), "Root1"));
            UpdateUI();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filemonger = FilemongerFactory.LoadFromFile(openFileDialog1.FileName);
                UpdateUI();
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                FilemongerFactory.SaveToFile(filemonger, saveFileDialog1.FileName);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            if (openFileDialog2.ShowDialog() == DialogResult.OK)
            {
                var fileNames = ReadFileNames(openFileDialog2.FileName).ToArray();
                progressBar1.Value = 0;
                progressBar1.Maximum = fileNames.Length;

                foreach (var fileName in fileNames)
                {
                    var fileEntry = filemonger.GetFileEntry(fileName + ".tst");
                    using (var s = fileEntry.CreateText())
                    {
                        s.WriteLine(fileEntry.FullName);
                        s.WriteLine(DateTime.Now);
                    }
                    progressBar1.PerformStep();
                }

                //string fileName;
                //while ((fileName = fs.ReadLine()) != null)
                //{
                //    var fileEntry = filemonger.GetFileEntry(fileName + ".tst");
                //    using (var s = fileEntry.CreateText())
                //    {
                //        s.WriteLine(fileEntry.FullName);
                //        s.WriteLine(DateTime.Now);
                //    }
                //}
            }
        }

        private static IEnumerable<string > ReadFileNames(string fileName)
        {
            using (var fs = File.OpenText(fileName))
            {
                string line;
                while ((line = fs.ReadLine()) != null)
                {
                    yield return line;
                }
            }
        }
    }
}
