﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Filemonger.Demo
{
    public partial class NewFilemonger : Form
    {
        private int mountPointCount = 0;
        private int topFolderCount = 0;
        private int subFolderCount = 0;

        public NewFilemonger()
        {
            InitializeComponent();

            mountPointCount = 1 << trackBar1.Value;
            topFolderCount = 1 << trackBar2.Value;
            subFolderCount = 1 << trackBar3.Value;

            UpdateUI();
        }

        private void UpdateUI()
        {
            label2.Text = mountPointCount.ToString();
            label3.Text = topFolderCount.ToString();
            label5.Text = subFolderCount.ToString();
            label7.Text = (mountPointCount * topFolderCount * subFolderCount).ToString();
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            mountPointCount = 1 << trackBar1.Value;
            UpdateUI();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {

            topFolderCount = 1 << trackBar2.Value;
            UpdateUI();
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            subFolderCount = 1 << trackBar3.Value;
            UpdateUI();
        }

        public IFilemonger CreateFilemonger()
        {
            return FilemongerFactory.Create((ushort)mountPointCount, (ushort)topFolderCount, (ushort)subFolderCount);
        }
    }
}
