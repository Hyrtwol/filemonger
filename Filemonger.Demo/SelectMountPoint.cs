﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Filemonger.Demo
{
    public partial class SelectMountPoint : Form
    {
        public SelectMountPoint(IEnumerable<string> mountPoints)
        {
            InitializeComponent();

            listBox1.BeginUpdate();
            foreach (var mountPoint in mountPoints)
            {
                listBox1.Items.Add(mountPoint);
            }
            listBox1.EndUpdate();
            button2.Enabled = false;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectedIndex = listBox1.SelectedIndex;
            //Debug.Print("SelectedIndex={0}", listBox1.SelectedIndex);
            button2.Enabled = SelectedIndex > -1;
        }

        public int SelectedIndex { get; private set; }
    }
}
