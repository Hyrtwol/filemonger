﻿using System.Diagnostics;
using System.Linq;
using NUnit.Framework;

namespace Filemonger.Oracle.Test
{
    [TestFixture]
    public class MediaFilesTests
    {
        private const string TnsAlias =
            "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=devdb.247ms.com)(PORT=1521))(CONNECT_DATA=(SID=INCMS01)))";

        private const string ConnectionString =
            "Data Source=" + TnsAlias + ";Persist Security Info=True;User ID=metatool;Password=metatool";

        [Test]
        public void GetFiles()
        {
            var repo = new Repo(ConnectionString);
            {
                foreach (var mediaSample in repo.GetFiles().Take(10))
                {
                    Debug.WriteLine(mediaSample);
                }
            }
        }
    }
}