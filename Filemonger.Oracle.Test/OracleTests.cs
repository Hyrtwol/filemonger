﻿using System.Data;
using System.Diagnostics;
using Filemonger.Oracle.Test.Properties;
using NUnit.Framework;
using Oracle.ManagedDataAccess.Client;

namespace Filemonger.Oracle.Test
{
    [TestFixture]
    public class OracleTests
    {
        //private const string ConnectionString =
            //"Data Source=DEV_INCMS01;Persist Security Info=True;User ID=metatool;Password=metatool";

        [Test]
        public void CanConnect()
        {
            using (var connection = new OracleConnection(Settings.Default.OraConStr))
            {
                Assert.AreEqual(ConnectionState.Closed, connection.State);
                connection.Open();
                Debug.Print("State={0}", connection.State);
                Debug.Print("ServerVersion={0}", connection.ServerVersion);
                Debug.Print("HostName={0}", connection.HostName);
                Debug.Print("DataSource={0}", connection.DataSource);
                Assert.AreEqual(ConnectionState.Open, connection.State);
                connection.Close();
                Assert.AreEqual(ConnectionState.Closed, connection.State);
            }
        }

        //[Test]
        //public void GetProvider()
        //{
        //    //var repo = new Repo();
        //    var conStr = Repo.GetConnectionStringByProvider("Oracle.DataAccess");
        //    Debug.Print("conStr={0}", conStr);
        //}
    }
}
