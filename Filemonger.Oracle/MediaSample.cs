﻿namespace Filemonger.Oracle
{
    public class MediaSample
    {
        public long Id;
        public string FileName;

        public override string ToString()
        {
            return string.Format("Sample {{ Id={0}, FileName={1} }}", Id, FileName);
        }
    }
}