﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Filemonger.Oracle.Properties;

namespace Filemonger.Oracle
{
    static class Program
    {
        private const int NumberOfFilesToAdd = 200000;

        static void Main()
       {
            Console.WriteLine(" -= Filemonger.Oracle =- ");
            Console.WriteLine();
            Console.WriteLine(Settings.Default.OraConStr);

            //DumpFileNames(); return;

            //var filemonger = new Filemonger(2, 4, 4);
            var filemonger = FilemongerFactory.Create(2, 4, 4);

            Console.WriteLine(filemonger.Mask.ToString());

            var mask = filemonger.Mask;
            var folderFileCount = new Dictionary<long, int>();
            for (int i = 0; i <= mask.MountPoint; i++)
            {
                for (int j = 0; j <= mask.TopFolder; j++)
                {
                    for (int k = 0; k <= mask.SubFolder; k++)
                    {
                        folderFileCount[GetFolderId(i, j, k)] = 0;
                    }
                }
            }

            //using (var repo = new Repo(Settings.Default.OraConStr))
            var repo = new Repo(Settings.Default.OraConStr);
            {
                int c = 0;
                Console.Write("Adding {0} from lmsamples", NumberOfFilesToAdd);
                foreach (var mediaSample in repo.GetFiles().Take(NumberOfFilesToAdd))
                {
                    if ((c++ & 0xFFF) == 0) Console.Write(".");
                    //Console.WriteLine(mediaSample);
                    var muid = filemonger.GetFileIndex(mediaSample.FileName);
                    long folderId = GetFolderId(muid);
                    folderFileCount[folderId]++;
                    //Console.WriteLine("{0} -> {1:X8}", mediaSample.FileName, folderId);
                }
                Console.WriteLine();

                //Console.WriteLine("{0}", int.MaxValue);
                foreach (var l in repo.GetIntegers())
                {
                    Console.WriteLine("{0}", l);
                }
            }

            Console.WriteLine("Folders:");
            //var data = new int[folderFileCount.Count];
            var data = folderFileCount.Values.ToArray();
            
            int idx = 0;
            foreach (var kv in folderFileCount)
            {
                //data[idx] = kv.Value;
                Console.WriteLine("  {0,4} : {1:X10} = {2}", idx++, kv.Key, kv.Value);
            }

            var statistics = new Statistics(data);
            Console.WriteLine("Statistics:");
            Console.WriteLine("  Total Files              = {0,8}", statistics.Sum);
            Console.WriteLine("  Total Folders            = {0,8}", statistics.Count);
            Console.WriteLine("  Minimum Files Per Folder = {0,8:F3}", statistics.Minimum);
            Console.WriteLine("  Maximum Files Per Folder = {0,8:F3}", statistics.Maximum);
            Console.WriteLine("  Average Files Per Folder = {0,8:F3}", statistics.Average);
            //Console.WriteLine("  Deviation = {0,8:F3}", statistics.SampleStandardDeviation);

            Console.Write("Done. ");
            Console.ReadKey();
        }

        private static void DumpFileNames()
        {
            //using (var repo = new Repo(Settings.Default.OraConStr))
            var repo = new Repo(Settings.Default.OraConStr);
            {
                var r = new Random();
                int bail = 10000;
                const int numberOfFilesToAdd = 1000000;

                using (var fs = File.CreateText(@"D:\Trash\FileMongerRoot\Filenames.txt"))
                {
                    foreach (var mediaSample in repo.GetFiles().Take(numberOfFilesToAdd))
                    {
                        if(r.Next(20) == 0)
                        {
                            fs.WriteLine(mediaSample.FileName);
                            if(--bail == 0) break;
                        }
                    }
                }
            }
        }

        private static long GetFolderId(Hash hash)
        {
            return GetFolderId(hash.MountPoint, hash.TopFolder, hash.SubFolder);
        }

        private static long GetFolderId(long mountPoint, long topFolder, long subFolder)
        {
            return (mountPoint << 32) + (topFolder << 16) + subFolder;
        }
    }
}
