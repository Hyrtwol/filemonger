﻿using System;
using System.Collections.Generic;
using System.Data;
using Oracle.ManagedDataAccess.Client;

namespace Filemonger.Oracle
{
    public class Repo //: IDisposable
    {
        private readonly string _connectionString;
        //private readonly OracleConnection connection;

        public string ConnectionString { get { return _connectionString; } }

        //public bool IsOpen { get { return connection.State == ConnectionState.Open; } }

        public Repo(string connectionString)
        {
            _connectionString = connectionString;
            //connection = new OracleConnection(connectionString);
            //Open();
        }

        private OracleConnection CreateConnection()
        {
            var connection = new OracleConnection(_connectionString);
            connection.Open();
            if (connection.State != ConnectionState.Open)
                throw new InvalidOperationException(string.Format(
                    "Connection is not {0} but was {1}", ConnectionState.Open, connection.State));
            return connection;
        }

        //public void Dispose()
        //{
        //    if (connection.State == ConnectionState.Open)
        //        Close();
        //    connection.Dispose();
        //}

        //public void Open()
        //{
        //    connection.Open();
        //}

        //public void Close()
        //{
        //    connection.Close();
        //}

        public IEnumerable<MediaSample> GetFiles()
        {
            const string sql =
                "select /*+ FIRST_ROWS */ ls.sample_id, ls.filename " +
                "from music_247.lmsamples ls where ls.sample_id > 1000000";

            using (var connection = CreateConnection())
            {
                using (var oracleCommand = new OracleCommand(sql, connection))
                {
                    var oracleDataReader = oracleCommand.ExecuteReader();
                    if (oracleDataReader.FieldCount != 2) throw new InvalidOperationException();

                    while (oracleDataReader.Read())
                    {
                        if (oracleDataReader.IsDBNull(0) || oracleDataReader.IsDBNull(1)) continue;
                        yield return new MediaSample
                        {
                            Id = Convert.ToInt64(oracleDataReader[0]),
                            //Id = oracleDataReader.GetInt64(0),
                            FileName = oracleDataReader.GetString(1)
                        };
                    }
                }
            }
        }

        public IEnumerable<string> GetFileNames()
        {
            const string sql =
                "select /*+ FIRST_ROWS */ ls.filename " +
                "from music_247.lmsamples ls where ls.sample_id > 100000";

            using (var connection = CreateConnection())
            {
                using (var oracleCommand = new OracleCommand(sql, connection))
                {
                    var oracleDataReader = oracleCommand.ExecuteReader();
                    if (oracleDataReader.FieldCount < 1) throw new InvalidOperationException();

                    while (oracleDataReader.Read())
                    {
                        if (oracleDataReader.IsDBNull(0)) continue;
                        yield return oracleDataReader.GetString(0);
                    }
                }
            }
        }

        public IEnumerable<Tuple<Int32, Int64>> GetIntegers()
        {
            const string sql = "select * from tlc.datatest t";

            using (var connection = CreateConnection())
            {
                using (var oracleCommand = new OracleCommand(sql, connection))
                {
                    var oracleDataReader = oracleCommand.ExecuteReader();
                    if (oracleDataReader.FieldCount < 1) throw new InvalidOperationException();

                    while (oracleDataReader.Read())
                    {
                        var c1 = oracleDataReader.GetInt64(0);
                        var c2 = oracleDataReader.GetInt32(1);
                        yield return new Tuple<Int32, Int64>(c2, c1);
                    }
                }
            }
        }
    }
}
