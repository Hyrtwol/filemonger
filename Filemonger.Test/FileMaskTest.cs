﻿using System.Diagnostics;
using NUnit.Framework;

namespace Filemonger.Test
{
    [TestFixture]
    public class FileMaskTest
    {
        [Test]
        public void Construct()
        {
            var muid = new FileMask(0x0001, 0x0002, 0x0003);
            Debug.WriteLine(muid);
            Assert.AreEqual(1, muid.MountPoint);
            Assert.AreEqual(2, muid.TopFolder);
            Assert.AreEqual(3, muid.SubFolder);
        }

        [Test]
        public void Equals()
        {
            var m1 = new FileIndex("aaa");
            var m2 = new FileIndex("aaa");
            var m3 = new FileIndex("bbb");
            Debug.WriteLine(m1);
            Debug.WriteLine(m2);
            Debug.WriteLine(m3);
            Assert.AreEqual(m1, m2);
            Assert.AreNotEqual(m1, m3);
        }

        [Test]
        public void Mask_2_4_4()
        {
            var mask = new FileMask(0x0001, 0x0003, 0x0003);
            Debug.WriteLine("mask   = " + mask);
            var muid = new FileIndex("blablablabla");
            Debug.WriteLine("muid   = " + muid);
            muid.Mask(mask);
            Debug.WriteLine("masked = " + muid);
            Assert.AreEqual(1, muid.MountPoint);
            Assert.AreEqual(3, muid.TopFolder);
            Assert.AreEqual(1, muid.SubFolder);
        }


        [Test]
        public void Mask_4_64_64()
        {
            var mask = new FileMask(3, 63, 63);
            Debug.WriteLine("mask   = " + mask);
            var muid = new FileIndex("myfancyfilename.mp3");
            Debug.WriteLine("muid   = " + muid);
            muid.Mask(mask);
            Debug.WriteLine("masked = " + muid);
            Assert.AreEqual(0, muid.MountPoint);
            Assert.AreEqual(33, muid.TopFolder);
            Assert.AreEqual(11, muid.SubFolder);
        }
        
    }
}