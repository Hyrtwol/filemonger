﻿using System;
using System.Diagnostics;
using System.Net;
using NUnit.Framework;

namespace Filemonger.Test
{
    [Ignore("Using remote url")]
    [TestFixture]
    public class FilemongerCacheTest
    {
        [Test]
        public void CreateFilemongerFromCache()
        {
            const string url = "http://filelocator.dev.247ms.com/flac";
            var filemongerCache = new FilemongerCache(url, 10.0);
            filemongerCache.CacheEvent += TestEvent;
            var filemonger1 = filemongerCache.GetFilemonger();
            Assert.AreEqual(url, filemongerCache.Url);
            Assert.IsNotNull(filemonger1);
            filemonger1.WriteToConsole();

            var filemonger2 = filemongerCache.GetFilemonger();
            Assert.IsNotNull(filemonger2);

            Assert.AreSame(filemonger1, filemonger2);
        }

        [Test]
        public void CreateFilemongerFromCustomCache()
        {
            const string url = "http://filelocator.dev.247ms.com/flac";
            var filemongerCache = new TestCache(url, 10.0);
            filemongerCache.CacheEvent += TestEvent;
            var filemonger1 = filemongerCache.GetFilemonger();
            Assert.IsNotNull(filemonger1);
            filemonger1.WriteToConsole();

            var filemonger2 = filemongerCache.GetFilemonger();
            Assert.IsNotNull(filemonger2);

            Assert.AreSame(filemonger1, filemonger2);
        }


        private void TestEvent(object sender, CacheEventArgs e)
        {
            Debug.WriteLine(e.Message);
        }

        class TestCache : FilemongerCache
        {
            public TestCache(string url, double seconds) : base(url, seconds)
            {
            }

            protected override IFilemonger CreateFilemongerFromUrl(string url)
            {
                using (var wc = new WebClient())
                {
                    wc.Headers.Add("Content-Type: application/json");
                    var json = wc.DownloadString(url);
                    //Debug.WriteLine(json);
                    WebHeaderCollection myWebHeaderCollection = wc.ResponseHeaders;
                    Debug.WriteLine("\nDisplaying the response headers\n");
                    // Loop through the ResponseHeaders and display the header name/value pairs. 
                    for (int i = 0; i < myWebHeaderCollection.Count; i++)
                        Debug.WriteLine("\t" + myWebHeaderCollection.GetKey(i) + " = " + myWebHeaderCollection.Get(i));

                    var recipe = FilemongerFactory.CreateRecipeFromJson(json);
                    return FilemongerFactory.Create(recipe);
                }
                //return base.CreateFilemongerFromUrl(url);
            }
        }
    }
}