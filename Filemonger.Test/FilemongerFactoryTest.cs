﻿using System;
using System.Diagnostics;
using System.IO;
using NUnit.Framework;

namespace Filemonger.Test
{
    [TestFixture]
    public class FilemongerFactoryTest
    {
        [Test]
        public void CreateFilemongerFromPath()
        {
            var path = Utils.GetFolderNameFromStackTrace();
            IO.DeleteDirectoryIfExists(path);

            var filemonger = FilemongerFactory.Create(4, 64, 64, //path);
                path + @"\00",
                path + @"\01",
                path + @"\02",
                path + @"\03");

            Assert.IsNotNull(filemonger);
            filemonger.WriteToConsole();
            filemonger.CreateAllMountPoints();
            Assert.IsTrue(Directory.Exists(path), string.Format("Directory not found {0}", path));
            //var fileInfo = filemonger.GetFileEntry("test1.txt");
            var fileInfo = filemonger.GetFileEntry("myfancyfilename.mp3");
            Assert.IsNotNull(fileInfo);
            //Debug.WriteLine(fileInfo.);
            fileInfo.WriteToConsole();
            fileInfo.CreateTestFile();
            filemonger.WriteFilesToConsole();
        }

        [Test]
        public void CreateFilemongerFromRecipe()
        {
            var path = Utils.GetFolderNameFromStackTrace();
            IO.DeleteDirectoryIfExists(path);

            var recipe = new FilemongerRecipe
            {
                MountPointPaths = new[] { path },
                MountPointBindings = new[] { 0, 0 }, 
                TopFolderCount = 2, SubFolderCount = 2
            };

            var filemonger = FilemongerFactory.Create(recipe);

            Assert.IsNotNull(filemonger);
            filemonger.WriteToConsole();
            filemonger.CreateAllMountPoints();
            Assert.IsTrue(Directory.Exists(path), string.Format("Directory not found {0}", path));
            var fileInfo = filemonger.GetFileEntry("test1.txt");
            Assert.IsNotNull(fileInfo);

            fileInfo.WriteToConsole();
            fileInfo.CreateTestFile();
            filemonger.WriteFilesToConsole();
        }

        [Test]
        public void CreateFilemongerFromRecipeXmlFile()
        {
            var path = Utils.GetFolderNameFromStackTrace();
            IO.DeleteDirectoryIfExists(path);

            var filemonger2 = FilemongerFactory.Create(1, 4, 4, path);
            FilemongerFactory.SaveToFile(filemonger2, Path.Combine(path, "CreateFilemongerFromRecipe.filemonger"));

            var filemonger = FilemongerFactory.LoadFromFile(Path.Combine(path, "CreateFilemongerFromRecipe.filemonger"));

            Assert.IsNotNull(filemonger);
            filemonger.WriteToConsole();
            filemonger.CreateAllMountPoints();
            Assert.IsTrue(Directory.Exists(path), string.Format("Directory not found {0}", path));
            var fileInfo = filemonger.GetFileEntry("test1.txt");
            Assert.IsNotNull(fileInfo);
            fileInfo.WriteToConsole();
            fileInfo.CreateTestFile();
            filemonger.WriteFilesToConsole();
        }

        [Test]
        public void CreateFilemongerFromRecipeJsonFile()
        {
            var path = Utils.GetFolderNameFromStackTrace();
            IO.DeleteDirectoryIfExists(path);

            var filemonger2 = FilemongerFactory.Create(2, 4, 4, Path.Combine(path, "mount0"), Path.Combine(path, "mount1"));
            FilemongerFactory.SaveToFile(filemonger2, Path.Combine(path, "CreateFilemongerFromRecipe.filemonger"), FilemongerRecipeFileType.Json);

            var filemonger = FilemongerFactory.LoadFromFile(Path.Combine(path, "CreateFilemongerFromRecipe.filemonger"), FilemongerRecipeFileType.Json);

            Assert.IsNotNull(filemonger);
            filemonger.WriteToConsole();
            filemonger.CreateAllMountPoints();
            Assert.IsTrue(Directory.Exists(path), string.Format("Directory not found {0}", path));
            var fileInfo = filemonger.GetFileEntry("test1.txt");
            Assert.IsNotNull(fileInfo);
            fileInfo.WriteToConsole();
            fileInfo.CreateTestFile();
            filemonger.WriteFilesToConsole();
        }

        [Ignore("Using remote url")]
        [Test]
        public void CreateFilemongerFromUrl()
        {
            const string url = "http://storage.dev.247ms.com/samples90";
            var filemonger = FilemongerFactory.LoadFromUrl(url);
            Assert.IsNotNull(filemonger);
            filemonger.WriteToConsole();
        }

    }
}