﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace Filemonger.Test
{
    [TestFixture]
    public class FilemongerTest
    {
        [Test]
        public void Construct()
        {
            var filemonger = new Filemonger(2, 4, 4);
            Assert.IsNotNull(filemonger);
            filemonger.WriteToConsole();
            Assert.AreEqual(2, filemonger.MountPointCount);
            Assert.AreEqual(4, filemonger.TopFolderCount);
            Assert.AreEqual(4, filemonger.SubFolderCount);
            Assert.AreEqual(32, filemonger.PotentialSubFolderCount);
        }

        [Test]
        public void GetRecipe()
        {
            var filemonger = new Filemonger(2, 4, 4);
            filemonger.Id = "-id-";
            Assert.IsNotNull(filemonger);
            filemonger.WriteToConsole();
            var recipe = filemonger.GetRecipe();
            Assert.AreEqual(2, recipe.MountPointCount);
            Assert.AreEqual(4, recipe.TopFolderCount);
            Assert.AreEqual(4, recipe.SubFolderCount);
            Assert.AreEqual("-id-", recipe.Id);
        }

        [Test]
        public void AddFiles()
        {
            var path = Utils.GetFolderNameFromStackTrace();
            IO.DeleteDirectoryIfExists(path);

            var filemonger = FilemongerFactory.Create(1, 4, 4, path);
            Assert.IsNotNull(filemonger);

            filemonger.WriteToConsole();

            filemonger.CreateAllMountPoints();
            Assert.IsTrue(Directory.Exists(path), string.Format("Directory not found {0}", path));

            var folderFileCountDictionary = GetFolderFileCountDictionary(filemonger.Mask);
            AddFilesAndCount(filemonger, folderFileCountDictionary, 10);

            Debug.WriteLine("Count:");
            int idx = 0;
            var data = new int[folderFileCountDictionary.Count];
            foreach (var kv in folderFileCountDictionary)
            {
                data[idx] = kv.Value;
                Debug.Print("  {0:D4} : {1:X8} = {2}", idx++, kv.Key, kv.Value);
            }            

            //filemonger.WriteFilesToConsole();

            var statistics = new Statistics(data);
            Debug.WriteLine("Statistics:");
            Debug.Print("  Minimum   = {0,8:F3}", statistics.Minimum);
            Debug.Print("  Maximum   = {0,8:F3}", statistics.Maximum);
            Debug.Print("  Average   = {0,8:F3}", statistics.Average);
            Debug.Print("  Deviation = {0,8:F3}", statistics.SampleStandardDeviation);
        }

        [Test]
        public void RenameFile()
        {
            var path = Utils.GetFolderNameFromStackTrace();
            IO.DeleteDirectoryIfExists(path);

            var filemonger = FilemongerFactory.Create(1, 4, 4, path);
            Assert.IsNotNull(filemonger);

            filemonger.WriteToConsole();

            filemonger.CreateAllMountPoints();
            Assert.IsTrue(Directory.Exists(path), string.Format("Directory not found {0}", path));

            var fileEntry = filemonger.GetFileEntry("test1.txt");
            Assert.IsNotNull(fileEntry);

            fileEntry.WriteToConsole();
            fileEntry.CreateTestFile();

            filemonger.WriteFilesToConsole();

            fileEntry.Rename("monger.dat");

            filemonger.WriteFilesToConsole();

            var oldFileEntry = filemonger.GetFileEntry("test1.txt");
            oldFileEntry.WriteToConsole();
            Assert.IsFalse(oldFileEntry.Exists);

            var newFileEntry = filemonger.GetFileEntry("monger.dat");
            newFileEntry.WriteToConsole();
            Assert.IsTrue(newFileEntry.Exists);

        }

        [Test]
        public void BoundMountPoints()
        {
            var sut = FilemongerFactory.Create("test", 4, 4, 4)
                .AddMountPoint(@"\\server\share0", 0)
                .AddMountPoint(@"\\server\share1", 1)
                .AddMountPoint(@"\\server\share0", 2)
                .AddMountPoint(@"\\server\share1", 3);
            Assert.IsNotNull(sut);
            Assert.AreEqual("test", sut.Id);
            var mps = sut.GetMountPoints().ToArray();
            Assert.AreEqual(4, mps.Length);
            var paths = sut.GetBoundMountPoints().ToArray();
            Assert.AreEqual(4, paths.Length);
            Assert.AreEqual(@"\\server\share0", paths[0]);
            Assert.AreEqual(@"\\server\share1", paths[1]);
            Assert.AreEqual(@"\\server\share0", paths[2]);
            Assert.AreEqual(@"\\server\share1", paths[3]);
        }

        private static void AddFilesAndCount(IFilemonger filemonger, IDictionary<int, int> folderFileCountDictionary, int count)
        {
            for (int z = 0; z < count; z++)
                for (int y = 0; y < count; y++)
                    for (int x = 0; x < count; x++)
                    {
                        var fileName = string.Format("{0:D4}_{1:D4}_{2:D4}.dat", x, y, z);
                        var muid = filemonger.GetFileIndex(fileName);
                        int folderId = GetFolderId(muid);
                        folderFileCountDictionary[folderId]++;

                        var fileEntry = filemonger.GetFileEntry(fileName);
                        Assert.IsNotNull(fileEntry);
                        fileEntry.CreateTestFile();
                    }
        }

        private static Dictionary<int, int> GetFolderFileCountDictionary(Hash mask)
        {
            var folderFileCount = new Dictionary<int, int>();
            for (int i = 0; i <= mask.MountPoint; i++)
            {
                for (int j = 0; j <= mask.TopFolder; j++)
                {
                    for (int k = 0; k <= mask.SubFolder; k++)
                    {
                        folderFileCount[GetFolderId(i, j, k)] = 0;
                    }
                }
            }
            return folderFileCount;
        }

        private static int GetFolderId(Hash hash)
        {
            return GetFolderId(hash.MountPoint, hash.TopFolder, hash.SubFolder);
        }

        private static int GetFolderId(int mountPoint, int topFolder, int subFolder)
        {
            return (mountPoint << 16) + (topFolder << 8) + subFolder;
        }
    }
}