﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using NUnit.Framework;

namespace Filemonger.Test
{
    [TestFixture]
    public class IOTest
    {
        [Test]
        public void DeleteDirectory()
        {
            const string path = "_" + "DeleteDirectory";
            Directory.CreateDirectory(path);
            Assert.IsTrue(Directory.Exists(path), string.Format("Directory not found {0}", path));
            IO.DeleteDirectoryIfExists(path);
            Assert.IsFalse(Directory.Exists(path), string.Format("Directory found {0}", path));
            IO.DeleteDirectoryIfExists(path);
        }

        [Test]
        public void IsPowerOfTwo()
        {
            for (int i = 0; i <= 16; i++)
            {
                Debug.Print("0x{0:X4} {1}", i, IO.IsPowerOfTwo(i));
            }
            Assert.IsFalse(IO.IsPowerOfTwo(0));
            Assert.IsTrue(IO.IsPowerOfTwo(1));
            Assert.IsTrue(IO.IsPowerOfTwo(2));
            Assert.IsFalse(IO.IsPowerOfTwo(3));
            Assert.IsTrue(IO.IsPowerOfTwo(4));
            Assert.IsFalse(IO.IsPowerOfTwo(5));
            Assert.IsFalse(IO.IsPowerOfTwo(6));
            Assert.IsFalse(IO.IsPowerOfTwo(7));
            Assert.IsTrue(IO.IsPowerOfTwo(8));
            Assert.IsFalse(IO.IsPowerOfTwo(9));
            Assert.IsFalse(IO.IsPowerOfTwo(10));
            Assert.IsFalse(IO.IsPowerOfTwo(11));
            Assert.IsFalse(IO.IsPowerOfTwo(12));
            Assert.IsFalse(IO.IsPowerOfTwo(13));
            Assert.IsFalse(IO.IsPowerOfTwo(14));
            Assert.IsFalse(IO.IsPowerOfTwo(15));
            Assert.IsTrue(IO.IsPowerOfTwo(16));
        }

#if DEBUG
        [Ignore("Using remote url")]
        [Test]
        public void WebClientDownloadString()
        {
            const string url = "http://filelocator.dev.247ms.com/mp4-320kbps";
            using (var wc = new WebClient())
            {
                wc.Headers.Add("Content-Type: application/json");
                var content = wc.DownloadString(url);
                Debug.WriteLine(content);
            }
        }

        [Ignore("Using remote url")]
        [Test]
        public void WebClientDownloadData()
        {
            const string url = "http://filelocator.dev.247ms.com/mp4-320kbps";
            using (var wc = new WebClient())
            {
                wc.Headers.Add("Content-Type: application/json");
                var content = wc.DownloadString(url);
                Debug.WriteLine(content);

                WebHeaderCollection myWebHeaderCollection = wc.ResponseHeaders;
                Console.WriteLine("\nDisplaying the response headers\n");
                // Loop through the ResponseHeaders and display the header name/value pairs. 
                for (int i = 0; i < myWebHeaderCollection.Count; i++)
                    Console.WriteLine("\t" + myWebHeaderCollection.GetKey(i) + " = " + myWebHeaderCollection.Get(i));

            }
        }
#endif
    }
}