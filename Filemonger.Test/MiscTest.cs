﻿using System;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;

namespace Filemonger.Test
{
    [TestFixture]
    public class MiscTest
    {
        [Test]
        public unsafe void Structs()
        {
            var buf = new byte[16];
            var mask = new Hash(buf);
            var index = new Index { MountPoint = 1, TopFolder = 2, SubFolder = 3 };
            Index* pindex = &index;

            fixed (void* pmask = buf)
            {
                ulong* ph = (ulong*)pmask;
                ulong* pm = (ulong*)pindex;
                *ph = *pm;
                ph++; pm++;
                *ph = *pm;
            }
            Debug.WriteLine(mask);

            Debug.WriteLine(index);

            var hash = new FileIndex("blablablabla");
            Debug.WriteLine(hash);

            fixed (byte* dest = &hash.Bytes[0])
            {
                index = *((Index*)(dest));
            }
            Debug.WriteLine(index);

            int size = sizeof(Index);
            Debug.Print("sizeof(Index) = {0}", size);

            var b = new byte[16];
            fixed (byte* dest = &b[0])
            {
                *((Index*)(dest)) = index;
            }
            Debug.WriteLine("1> {0}", new Hash(b));

            index.MountPoint = 1;
            index.TopFolder = 2;
            index.SubFolder = 3;

            fixed (byte* dest = &b[0])
            {
                *((Index*)(dest)) = index;
            }
            Debug.Print("2> {0}", new Hash(b));
        }

        [Test]
        public void UInt16()
        {
            for (short i = -3; i <= 3; i++)
            {
                Debug.Print("{0,3:D2} :: 0x{0:X4} :: 0x{1:X4}", i, (short)~i, (short)(i - 1));
            }
        }

        [Test]
        public void TakeIt()
        {
            var ary = new[] {1, 2, 3, 4};

            foreach (var i in ary) Debug.WriteLine(i);
            foreach (var i in ary.Take(8)) Debug.WriteLine(i);

            Assert.AreEqual(2, ary.Take(2).Count());
            Assert.AreEqual(4, ary.Take(8).Count());
        }
    }
}