﻿using System.Diagnostics;
using NUnit.Framework;

namespace Filemonger.Test
{
    [TestFixture]
    public class StatisticsTest
    {
        [Test]
        public void VerifyStatistics()
        {
            var data = new[] {2, -1, 2, 3, 0, 25, -1, 2};
            var statistics = new Statistics(data);
            Debug.Print("minimum                       = {0,8:F3}", statistics.Minimum);
            Debug.Print("maximum                       = {0,8:F3}", statistics.Maximum);
            Debug.Print("mean                          = {0,8:F3}", statistics.Average);
            Debug.Print("median                        = {0,8:F3}", statistics.Median);
            Debug.Print("range                         = {0,8:F3}", statistics.Range);

            Debug.Print("sample standard deviation     = {0,8:F3}", statistics.SampleStandardDeviation);
            Debug.Print("sample variance               = {0,8:F3}", statistics.SampleVariance);
            Debug.Print("population standard deviation = {0,8:F3}", statistics.PopulationStandardDeviation);
            Debug.Print("population variance           = {0,8:F3}", statistics.PopulationVariance);

            Assert.AreEqual(-1.0, statistics.Minimum);
            Assert.AreEqual(25.0, statistics.Maximum);
            Assert.AreEqual(4.0, statistics.Average);
            Assert.AreEqual(2.0, statistics.Median);
            Assert.AreEqual(26.0, statistics.Range);
            Assert.AreEqual(8.618916073713347d, statistics.SampleStandardDeviation);
            Assert.AreEqual(8.0622577482985491d, statistics.PopulationStandardDeviation);
        }
    }
}