﻿using System;
using System.Diagnostics;

namespace Filemonger.Test
{
    internal static class Utils
    {
        internal static string GetFolderNameFromStackTrace()
        {
            var stackTrace = new StackTrace();
            var stackFrame = stackTrace.GetFrame(1);
            var method = stackFrame.GetMethod();
            return "_" + method.DeclaringType.Name + "_" + method.Name;
        }

        internal static void WriteToConsole(this IFilemonger filemonger)
        {
            Debug.WriteLine("Filemonger:");
            Debug.Print("  Id:              {0}", filemonger.Id);
            Debug.Print("  MountPointCount: {0}", filemonger.MountPointCount);
            Debug.Print("  TopFolderCount:  {0}", filemonger.TopFolderCount);
            Debug.Print("  SubFolderCount:  {0}", filemonger.SubFolderCount);
            Debug.Print("  Mask:            {0}", filemonger.Mask);
            Debug.Print("  Exists:          {0}", filemonger.Exists);
            foreach (var mountPoint in filemonger.GetMountPoints())
            {
                Debug.Print("    MountPoint:    {0}", mountPoint);
            }
        }

        internal static void WriteFilesToConsole(this IFilemonger filemonger)
        {
            Debug.WriteLine("Files:");
            foreach (var fileInfo in filemonger.EnumerateFiles())
            {
                Debug.Print("  {0,-16} {1}", fileInfo, fileInfo.Directory);
            }
        }

        internal static void WriteToConsole(this FileEntry fileEntry)
        {
            Debug.WriteLine("FileEntry:");
            Debug.Print("  {0,-16} {1}", fileEntry.Name, fileEntry.DirectoryName);
        }

        internal static void CreateTestFile(this FileEntry fileEntry)
        {
            using (var s = fileEntry.CreateText())
            {
                s.WriteLine(DateTime.Now);
                s.WriteLine(fileEntry.FullName);
            }
        }
    }
}