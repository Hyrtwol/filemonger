﻿using System;

namespace Filemonger
{
    public class CacheEventArgs : EventArgs
    {
        public CacheEventArgs(string message)
        {
            Message = message;
        }

        public string Message { get; private set; }
    }
}