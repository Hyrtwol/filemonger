﻿using System;
using System.IO;

namespace Filemonger
{
    public class FileEntry
    {
        private readonly IFilemonger _owner;
        private readonly string _path;
        private readonly string _name;

        internal FileEntry(IFilemonger owner, string path, string name)
        {
            _owner = owner;
            _path = path;
            _name = name;
        }

        public string Name
        {
            get { return _name; }
        }

        public string FullName
        {
            get { return Path.Combine(_path, _name); }
        }

        public string DirectoryName
        {
            get { return _path; }
        }

        public string FilemongerId
        {
            get { return _owner != null ? _owner.Id : null; }
        }

        public Uri Uri
        {
            get { return new Uri(FullName); }
        }

        public bool Exists
        {
            get { return File.Exists(FullName); }
        }

        public FileStream Create()
        {
            return File.Create(FullName);
        }

        public FileStream OpenRead()
        {
            return File.OpenRead(FullName);
        }

        public FileStream OpenWrite()
        {
            return File.OpenWrite(FullName);
        }

        public StreamWriter CreateText()
        {
            return File.CreateText(FullName);
        }

        public StreamReader OpenText()
        {
            return File.OpenText(FullName);
        }

        public void CopyTo(string destFileName)
        {
            File.Copy(FullName, destFileName);
        }

        public void CopyTo(string destFileName, bool overwrite)
        {
            File.Copy(FullName, destFileName, overwrite);
        }

        public void Delete()
        {
            File.Delete(FullName);
        }

        public FileEntry Rename(string newFileName)
        {
            var newFileEntry = _owner.GetFileEntry(newFileName);
            if (newFileEntry.Exists) throw new InvalidOperationException("File already exists");
            File.Move(FullName, newFileEntry.FullName);
            return newFileEntry;
        }
    }
}