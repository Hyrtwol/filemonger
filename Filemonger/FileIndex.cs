﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Filemonger
{
    public class FileIndex : Hash
    {
        public FileIndex(byte[] input)
            : base(ComputeHash(input))
        {
        }

        public FileIndex(string input)
            : base(ComputeHash(input))
        {
        }

        public FileIndex(byte[] input, Hash mask)
            : base(ComputeHash(input), mask)
        {
        }

        public FileIndex(string input, Hash mask)
            : base(ComputeHash(input), mask)
        {
        }

        private static byte[] ComputeHash(byte[] input)
        {
            if (input == null) throw new ArgumentNullException("input");
            using (var md5 = MD5.Create())
            {
                return md5.ComputeHash(input);
            }
        }

        private static byte[] ComputeHash(string input)
        {
            if (string.IsNullOrEmpty(input)) throw new ArgumentNullException("input");
            return ComputeHash(Encoding.ASCII.GetBytes(input));
        }
    }
}