﻿using System;

namespace Filemonger
{
    public class FileMask : Hash
    {
        public FileMask(ushort mountPoint, ushort topFolder, ushort subFolder)
            : base(ComputeMask(mountPoint, topFolder, subFolder))
        {
        }

#if USE_UNSAFE // doing unsafe just for fun and speed :p
        // properly the "safe" method can be written better and avoid 3 foreach
        private static unsafe byte[] ComputeMask(ushort mountPoint, ushort topFolder, ushort subFolder)
        {
            var index = new Index { MountPoint = mountPoint, TopFolder = topFolder, SubFolder = subFolder };
            var mask = new byte[HashByteCount];
            fixed (byte* dest = &mask[0])
            {
                *((Index*)(dest)) = index;
            }
            return mask;
        }
#else
        private static byte[] ComputeMask(ushort mountPoint, ushort topFolder, ushort subFolder)
        {
            var mask = new byte[HashByteCount];
            int i = 0;
            foreach (var b in BitConverter.GetBytes(mountPoint))
            {
                mask[i++] = b;
            }
            foreach (var b in BitConverter.GetBytes(topFolder))
            {
                mask[i++] = b;
            }
            foreach (var b in BitConverter.GetBytes(subFolder))
            {
                mask[i++] = b;
            }
            return mask;
        }
#endif
    }
}