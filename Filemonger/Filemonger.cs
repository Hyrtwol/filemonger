﻿using System;

namespace Filemonger
{
    internal class Filemonger : FilemongerBase
    {
        internal Filemonger(int mountPointCount, int topFolderCount, int subFolderCount, params string[] paths)
            : base(mountPointCount, topFolderCount, subFolderCount, paths)
        {
        }

        internal Filemonger(FilemongerRecipe recipe)
            : base(recipe)
        {
        }
    }
}