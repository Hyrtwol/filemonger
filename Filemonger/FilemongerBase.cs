﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Filemonger
{
    // ReSharper disable MemberCanBePrivate.Global
    // ReSharper disable UnusedMember.Global

    public abstract class FilemongerBase : IFilemonger
    {
        private readonly List<MountPoint> _mountPoints = new List<MountPoint>();
        private readonly int[] _mountPointBindings;
        private readonly int _mountPointCount;
        private readonly int _subFolderCount;
        private readonly int _topFolderCount;

        protected FilemongerBase(int mountPointCount, int topFolderCount, int subFolderCount)
        {
            if (mountPointCount < 1) throw new ArgumentOutOfRangeException(nameof(mountPointCount), "Must be bigger than 0");
            if (!IO.IsPowerOfTwo(mountPointCount))
                throw new ArgumentException("Value must be power of 2", nameof(mountPointCount));

            if (topFolderCount < 1) throw new ArgumentOutOfRangeException(nameof(topFolderCount), "Must be bigger than 0");
            if (!IO.IsPowerOfTwo(topFolderCount))
                throw new ArgumentException("Value must be power of 2", nameof(topFolderCount));

            if (subFolderCount < 1) throw new ArgumentOutOfRangeException(nameof(subFolderCount), "Must be bigger than 0");
            if (!IO.IsPowerOfTwo(subFolderCount))
                throw new ArgumentException("Value must be power of 2", nameof(subFolderCount));

            _mountPointCount = mountPointCount;
            _topFolderCount = topFolderCount;
            _subFolderCount = subFolderCount;

            Mask = new FileMask((ushort) (_mountPointCount - 1), (ushort) (_topFolderCount - 1), (ushort) (_subFolderCount - 1));
            PathFormat = "{1}{0}{2:x2}{0}{3:x2}";
        }

        protected FilemongerBase(int mountPointCount, int topFolderCount, int subFolderCount, params string[] paths)
            : this(mountPointCount, topFolderCount, subFolderCount)
        {
            var cnt = paths.Length;
            if (cnt > mountPointCount) throw new ArgumentOutOfRangeException("paths", "Too many paths. Maximun is the number of mount points");
            _mountPointBindings = new int[mountPointCount];
            for (int i = 0; i < mountPointCount; i++)
            {
                _mountPointBindings[i] = cnt > 0 ? i % cnt : 0;
            }
            foreach (var path in paths)
            {
                AddMountPoint(path);
            }
        }

        protected FilemongerBase(FilemongerRecipe recipe)
            : this(recipe.MountPointCount, recipe.TopFolderCount, recipe.SubFolderCount)
        {
            Id = recipe.Id;
            foreach (var mountPoint in recipe.MountPointPaths)
            {
                AddMountPoint(mountPoint);
                //AddMountPoint(mountPoint.LocalPath);
            }
            //mountPointBindings = recipe.MountPointBindings;
            int cnt = recipe.MountPointBindings.Length;
            _mountPointBindings = new int[cnt];
            for (int i = 0; i < cnt; i++)
            {
                BindMountPoint(i, recipe.MountPointBindings[i]);
            }
        }

        public string Id { get; set; }

        /// <summary>
        /// e.g. {1}{0}{2:X2}{0}{3:X2}
        /// {0} = Path.DirectorySeparatorChar
        /// {1} = mount point path
        /// {2} = top folder index
        /// {3} = sub folder index
        /// </summary>
        public string PathFormat { get; set; }

        public Hash Mask { get; private set; }

        public int MountPointCount
        {
            get { return _mountPointCount; }
        }

        public int TopFolderCount
        {
            get { return _topFolderCount; }
        }

        public int SubFolderCount
        {
            get { return _subFolderCount; }
        }

        public int PotentialSubFolderCount
        {
            get { return _mountPointCount*_topFolderCount*_subFolderCount; }
        }

        public int AvailableMountPointCount
        {
            get { return _mountPoints.Count; }
        }

        public bool Exists
        {
            get
            {
                return _mountPoints.Count > 0 && _mountPoints.All(mountPoint => mountPoint.Exists);
            }
        }

        public IFilemonger BindMountPoint(int mountPointIndex, int mountPoint)
        {
            if (mountPoint < 0 || mountPoint >= _mountPoints.Count) throw new ArgumentOutOfRangeException(nameof(mountPoint));
            _mountPointBindings[mountPointIndex] = mountPoint;
            return this;
        }

        public IFilemonger AddMountPoint(string path)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException();
            var mountPoint = new MountPoint(path);
            _mountPoints.Add(mountPoint);
            //CreateMountPoint(mountPoint);
            return this;
        }

        public IFilemonger AddMountPoint(string path, int mountPointIndex)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException();
            AddMountPoint(path);
            BindMountPoint(mountPointIndex, _mountPoints.Count - 1);
            return this;
        }

        public void CreateAllMountPoints()
        {
            foreach (var mountPoint in _mountPoints)
            {
                CreateMountPoint(mountPoint);
            }
        }

        //public override string ToString()
        //{
        //    var m = mountPoints.Select(mp => mp.FullPath).ToArray();
        //    return string.Join(";", m);
        //}

        /*
        public void Create(DirectorySecurity directorySecurity = null)
        {
            foreach (var mountPoint in mountPoints)
            {
                CreateMountPoint(mountPoint, directorySecurity);
            }
        }
        */

        private void CreateMountPoint(MountPoint mountPoint)
        {
            mountPoint.CreateIfNotExists();
            for (ushort topFolder = 0; topFolder < _topFolderCount; topFolder++)
            {
                for (ushort subFolder = 0; subFolder < _subFolderCount; subFolder++)
                {
                    var path = GetPath(mountPoint, topFolder, subFolder);
                    Directory.CreateDirectory(path);
                }
            }
        }

        public void Delete()
        {
            foreach (var mountPoint in _mountPoints)
            {
                mountPoint.Delete();
            }
        }

        public FileEntry GetFileEntry(string fileName)
        {
            var muid = GetFileIndex(fileName);
            var path = GetPath(muid.MountPoint, muid.TopFolder, muid.SubFolder);
            return new FileEntry(this, path, fileName);
        }

        public IEnumerable<FileInfo> EnumerateFiles(string searchPattern = "*")
        {
            return _mountPoints.SelectMany(mountPoint => mountPoint.EnumerateFiles(searchPattern));
        }

        public IEnumerable<string> GetMountPoints()
        {
            return _mountPoints.Select(mountPoint => mountPoint.FullPath);
        }

        public IEnumerable<int> GetMountPointBindings()
        {
            return _mountPointBindings;
        }

        public IEnumerable<string> GetBoundMountPoints()
        {
            if (_mountPointBindings.Length > 0)
            foreach (var mountPointBinding in _mountPointBindings)
            {
                var mp = mountPointBinding < _mountPoints.Count ? _mountPoints[mountPointBinding] : null;
                yield return mp != null ? mp.FullPath : null;
            }
        }

        public string GetPath(ushort mountPointIndex, ushort topFolderIndex, ushort subFolderIndex)
        {
            var index = _mountPointBindings[mountPointIndex];
            var mountPoint = _mountPoints[index];
            if (mountPoint == null) throw new NullReferenceException();
            return GetPath(mountPoint, topFolderIndex, subFolderIndex);
        }

        private string GetPath(MountPoint mountPoint, ushort topFolder, ushort subFolder)
        {
            return string.Format(PathFormat, Path.DirectorySeparatorChar, mountPoint, topFolder, subFolder);
        }

        public IEnumerable<string> GetTopFolders()
        {
            for (int i = 0; i < _topFolderCount; i++)
            {
                yield return string.Format("{0:X2}", i);
            }
        }

        public IEnumerable<string> GetSubFolders()
        {
            for (int i = 0; i < _subFolderCount; i++)
            {
                yield return string.Format("{0:X2}", i);
            }
        }

        public Hash GetFileIndex(string fileName)
        {
            return new FileIndex(fileName, Mask);
        }

        public override string ToString()
        {
            return string.Format("Filemonger {{ MountPointCount={0}, TopFolderCount={1}, SubFolderCount={2} }}", _mountPointCount, _topFolderCount, _subFolderCount);
        }

        public FilemongerRecipe GetRecipe()
        {
            return new FilemongerRecipe
            {
                Id = Id,
                TopFolderCount = (ushort)_topFolderCount,
                SubFolderCount = (ushort)_subFolderCount,
                MountPointPaths = GetMountPoints().ToArray(),
                //MountPointPaths = GetMountPoints().Select(mp => new Uri(mp)).ToArray(),
                MountPointBindings = _mountPointBindings.ToArray()
            };
        }
    }

    // ReSharper restore UnusedMember.Global
    // ReSharper restore MemberCanBePrivate.Global
}