﻿using System;

namespace Filemonger
{
    public class FilemongerCache
    {
        private static readonly object SyncRoot = new object();

        private readonly string _url;
        private readonly TimeSpan _ttl;

        private IFilemonger _filemonger;
        private DateTime _death;

        public event EventHandler<CacheEventArgs> CacheEvent;
        
        public FilemongerCache(string url, TimeSpan ttl)
        {
            _url = url;
            _ttl = ttl;
            _death = DateTime.Now.Add(_ttl);
        }

        public FilemongerCache(string url, double seconds)
            : this(url, TimeSpan.FromSeconds(seconds))
        {
        }

        public FilemongerCache(string url)
            : this(url, FilemongerSettings.FilemongerCacheTime)
        {
        }

        public FilemongerCache()
            : this(FilemongerSettings.FilemongerUrl)
        {
        }

        public string Url { get { return _url; } }

        public IFilemonger GetFilemonger()
        {
            lock (SyncRoot)
            {
                if (_filemonger == null || _death < DateTime.Now)
                {
                    try
                    {
                        var newFilemonger = GetFilemongerFromUrl();
                        if (newFilemonger != null)
                        {
                            _filemonger = newFilemonger;
                            OnRaiseCacheEvent(new CacheEventArgs("Filemonger was killed and replaced"));
                        }
                    }
                    catch (Exception ex)
                    {
                        OnRaiseCacheEvent(new CacheEventArgs("Error updating Filemonger " + ex.Message));
                    }
                    _death = DateTime.Now.Add(_ttl);
                }
            }
            return _filemonger;
        }

        private IFilemonger GetFilemongerFromUrl()
        {
            //var url = FilemongerSettings.FilemongerUrl;
            var filemonger = CreateFilemongerFromUrl(_url);
            //Debug.Print("Filemonger created from {0}", _url);
            OnRaiseCacheEvent(new CacheEventArgs("Filemonger created from "+ _url));

            return filemonger;
        }

        protected virtual IFilemonger CreateFilemongerFromUrl(string url)
        {
            return FilemongerFactory.LoadFromUrl(_url);
        }

        // Wrap event invocations inside a protected virtual method
        // to allow derived classes to override the event invocation behavior
        protected virtual void OnRaiseCacheEvent(CacheEventArgs e)
        {
            // Make a temporary copy of the event to avoid possibility of
            // a race condition if the last subscriber unsubscribes
            // immediately after the null check and before the event is raised.
            EventHandler<CacheEventArgs> handler = CacheEvent;

            // Event will be null if there are no subscribers
            if (handler != null)
            {
                // Format the string to send inside the CacheEventArgs parameter
                //e.Message += String.Format(" at {0}", DateTime.Now.ToString());

                // Use the () operator to raise the event.
                handler(this, e);
            }
        }

    }
}