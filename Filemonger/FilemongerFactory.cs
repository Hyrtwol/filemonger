﻿using System;
using System.IO;

namespace Filemonger
{
    public static class FilemongerFactory
    {
        public static IFilemonger Create(string id, int mountPointCount, int topFolderCount, int subFolderCount, params string[] paths)
        {
            var filemonger = Create((ushort) mountPointCount, (ushort) topFolderCount, (ushort) subFolderCount, paths);
            filemonger.Id = id;
            return filemonger;
        }

        public static IFilemonger Create(int mountPointCount, int topFolderCount, int subFolderCount, params string[] paths)
        {
            return Create((ushort)mountPointCount, (ushort)topFolderCount, (ushort)subFolderCount, paths);
        }

        public static IFilemonger Create(ushort mountPointCount, ushort topFolderCount, ushort subFolderCount, params string[] paths)
        {
            return new Filemonger(mountPointCount, topFolderCount, subFolderCount, paths);
        }

        public static IFilemonger Create(FilemongerRecipe recipe)
        {
            if (recipe == null) throw new ArgumentNullException("recipe");
            return new Filemonger(recipe);
        }

        public static IFilemonger Create(string path)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException("path");
            return Create(1, 4, 4, path);
        }

        public static void SaveToFile(IFilemonger filemonger, string fileName, FilemongerRecipeFileType fileType = FilemongerRecipeFileType.Xml)
        {
            if (filemonger == null) throw new ArgumentNullException("filemonger");
            if (string.IsNullOrEmpty(fileName)) throw new ArgumentNullException("fileName");
            var recipe = filemonger.GetRecipe();

            IO.CreateDirectoryIfNotExists(Path.GetDirectoryName(fileName));
            switch (fileType)
            {
                case FilemongerRecipeFileType.Xml:
                    IO.SerializeToXmlFile(recipe, fileName);
                    return;
                case FilemongerRecipeFileType.Json:
                    IO.SerializeToJsonFile(recipe, fileName);
                    return;
                default:
                    throw new InvalidOperationException("Invalid FilemongerRecipeFileType " + fileType);
            }
        }

        public static IFilemonger LoadFromFile(string fileName, FilemongerRecipeFileType fileType = FilemongerRecipeFileType.Xml)
        {
            if (string.IsNullOrEmpty(fileName)) throw new ArgumentNullException("fileName");

            FilemongerRecipe recipe;
            switch (fileType)
            {
                case FilemongerRecipeFileType.Xml:
                    recipe = IO.DeserializeFromXmlFile<FilemongerRecipe>(fileName);
                    break;
                case FilemongerRecipeFileType.Json:
                    recipe = IO.DeserializeFromJsonFile<FilemongerRecipe>(fileName);
                    break;
                default:
                    throw new InvalidOperationException("Invalid FilemongerRecipeFileType " + fileType);
            }
            return Create(recipe);
        }

        public static IFilemonger LoadFromUrl(string url)
        {
            if (string.IsNullOrEmpty(url)) throw new ArgumentNullException("url");
            var recipe = IO.RequestJsonFromUrl<FilemongerRecipe>(url);
            var filemonger = Create(recipe);
            return filemonger;
        }

        public static FilemongerRecipe CreateRecipeFromJson(string json)
        {
            if (string.IsNullOrEmpty(json)) throw new ArgumentNullException("json");
            return IO.DeserializeJson<FilemongerRecipe>(json);
        }
    }
}