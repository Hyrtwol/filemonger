﻿using System;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Filemonger
{
    public class FilemongerRecipe
    {
        public string Id;

        [XmlIgnore]
        [JsonIgnore]
        public int MountPointCount { get { return MountPointBindings != null ? MountPointBindings.Length : 0; } }

        public int TopFolderCount;

        public int SubFolderCount;

        [XmlArray("MountPointPaths")]
        [XmlArrayItem("MountPointPath")]
        public string[] MountPointPaths;

        [XmlArray("MountPointBindings")]
        [XmlArrayItem("MountPointIndex")]
        public int[] MountPointBindings;
        
    }
}