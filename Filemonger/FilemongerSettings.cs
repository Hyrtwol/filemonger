﻿using System;
using System.Configuration;
using System.Diagnostics;

namespace Filemonger
{
    public static class FilemongerSettings
    {
        private static readonly Lazy<TimeSpan> CacheTime =
            new Lazy<TimeSpan>(() =>
            {
                var cacheTimeString = ConfigurationManager.AppSettings["FilemongerCacheTime"];
                Debug.Print("{0} {1}", "FilemongerCacheTime", cacheTimeString);
                return TimeSpan.Parse(cacheTimeString);
            });

        public static TimeSpan FilemongerCacheTime { get { return CacheTime.Value; } }

        private static readonly Lazy<string> Url =
            new Lazy<string>(() =>
            {
                var filemongerUrl = ConfigurationManager.AppSettings["FilemongerUrl"];
                Debug.Print("{0} {1}", "FilemongerUrl", filemongerUrl);
                return filemongerUrl;
            });

        public static string FilemongerUrl { get { return Url.Value; } }
    }
}