﻿using System;
using System.Linq;
using System.Text;

namespace Filemonger
{
    public class Hash : IEquatable<Hash>
    {
        protected const int HashByteCount = 16; // 128 bit
        private const int MountPointIndex = 0;
        private const int TopFolderIndex = 2;
        private const int SubFolderIndex = 4;

        private readonly byte[] _hash;

        public byte[] Bytes { get { return (byte[]) _hash.Clone(); } }

        public ushort MountPoint { get { return BitConverter.ToUInt16(_hash, MountPointIndex); } }
        public ushort TopFolder { get { return BitConverter.ToUInt16(_hash, TopFolderIndex); } }
        public ushort SubFolder { get { return BitConverter.ToUInt16(_hash, SubFolderIndex); } }

        public Hash(byte[] hash)
        {
            if (hash == null) throw new ArgumentNullException("hash");
            if (hash.Length != HashByteCount)
                throw new ArgumentOutOfRangeException("hash", "hash length must be " + HashByteCount);
            _hash = hash;
        }

        public Hash(byte[] hash, Hash mask)
            : this(hash)
        {
            if (mask == null) throw new ArgumentNullException("mask");
            Mask(mask);
        }

#if USE_UNSAFE // doing unsafe just for fun and speed :p
        public unsafe void Mask(Hash mask)
        {
            fixed (byte* phash = _hash, pmask = mask._hash)
            {
                var ph = (ulong*)phash;
                var pm = (ulong*)pmask;
                *ph &= *pm;
                ph++; pm++;
                *ph &= *pm;
            }
        }
#else
        public void Mask(Hash mask)
        {
            for (int i = 0; i < HashByteCount; i++)
            {
                _hash[i] &= mask._hash[i];
            }
        }
#endif

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("{");
            foreach (var b in _hash)
            {
                sb.AppendFormat("{0:X2}", b);
            }
            sb.Append("}");
            return sb.ToString();
        }

        public bool Equals(Hash other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return other._hash.SequenceEqual(_hash);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            //if (obj.GetType() != typeof (Hash)) return false;
            var objAsHash = obj as Hash;
            if (objAsHash == null) return false;
            return Equals(objAsHash);
        }

        public override int GetHashCode()
        {
            return _hash.GetHashCode();
        }
    }
}