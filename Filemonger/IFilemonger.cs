﻿using System.Collections.Generic;
using System.IO;

namespace Filemonger
{
    public interface IFilemonger
    {
        string Id { get; set; }
        /// <summary>
        /// e.g. {1}{0}{2:X2}{0}{3:X2}
        /// {0} = Path.DirectorySeparatorChar
        /// {1} = mount point path
        /// {2} = top folder index
        /// {3} = sub folder index
        /// </summary>
        string PathFormat { get; set; }

        Hash Mask { get; }
        int MountPointCount { get; }
        int TopFolderCount { get; }
        int SubFolderCount { get; }
        int PotentialSubFolderCount { get; }
        int AvailableMountPointCount { get; }
        bool Exists { get; }
        IFilemonger AddMountPoint(string path);
        IFilemonger AddMountPoint(string path, int mountPointIndex);
        IFilemonger BindMountPoint(int mountPointIndex, int mountPoint);
        void CreateAllMountPoints();
        void Delete();
        FileEntry GetFileEntry(string fileName);
        IEnumerable<FileInfo> EnumerateFiles(string searchPattern = "*");
        IEnumerable<string> GetMountPoints();
        IEnumerable<int> GetMountPointBindings();
        IEnumerable<string> GetBoundMountPoints();
        string GetPath(ushort mountPointIndex, ushort topFolderIndex, ushort subFolderIndex);
        IEnumerable<string> GetTopFolders();
        IEnumerable<string> GetSubFolders();
        Hash GetFileIndex(string fileName);
        string ToString();
        FilemongerRecipe GetRecipe();
    }
}