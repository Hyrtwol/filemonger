﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Filemonger
{
    public static class IO
    {
        public static void DeleteDirectory(string path)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException("path");
            Debug.Print("Deleting {0}", path);
            Directory.Delete(path, true);
        }

        public static void DeleteDirectoryIfExists(string path)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException("path");
            if (Directory.Exists(path)) DeleteDirectory(path);
        }

        public static void CreateDirectoryIfNotExists(string path)
        {
            if (string.IsNullOrEmpty(path)) throw new ArgumentNullException("path");
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
        }

        public static bool IsPowerOfTwo(int value)
        {
            return (value != 0) && ((value - 1) & value) == 0;
        }

        public static T RequestJsonFromUrl<T>(string url)
        {
            var json = RequestJsonFromUrl(url);
            return DeserializeJson<T>(json);
        }

        public static string RequestJsonFromUrl(string url)
        {
            using (var wc = new WebClient())
            {
                wc.Headers.Add("Content-Type: application/json");
                return wc.DownloadString(url);
            }
        }

        public static T DeserializeJson<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static string SerializeJson<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        public static T DeserializeFromJsonFile<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) throw new ArgumentNullException("fileName");
            var json = File.ReadAllText(fileName);
            return DeserializeJson<T>(json);
        }

        public static void SerializeToJsonFile<T>(T obj, string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) throw new ArgumentNullException("fileName");
            var json = JsonConvert.SerializeObject(obj, Formatting.Indented);
            File.WriteAllText(fileName, json);
        }

        public static T DeserializeFromXmlFile<T>(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) throw new ArgumentNullException("fileName");
            using (var fs = File.OpenText(fileName))
            {
                var xmlSerializer = new XmlSerializer(typeof(T));
                var obj = xmlSerializer.Deserialize(fs);
                return (T)obj;
            }
        }

        public static void SerializeToXmlFile<T>(T obj, string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) throw new ArgumentNullException("fileName");
            using (var fs = File.CreateText(fileName))
            {
                var xmlSerializer = new XmlSerializer(typeof(T));
                xmlSerializer.Serialize(fs, obj);
            }
        }
    }
}
