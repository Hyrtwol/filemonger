﻿using System.Runtime.InteropServices;

namespace Filemonger
{
    [StructLayout(LayoutKind.Sequential, Size = 16, CharSet = CharSet.Ansi)]
    internal struct Index
    {
        public ushort MountPoint;
        public ushort TopFolder;
        public ushort SubFolder;

        public override string ToString()
        {
            return string.Format("Index: {0}/{1}/{2}", MountPoint, TopFolder, SubFolder);
        }
    }
}