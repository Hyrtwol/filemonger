﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Filemonger
{
    public class MountPoint
    {
        private readonly DirectoryInfo _directoryInfo;

        public MountPoint(string path)
        {
            if(string.IsNullOrEmpty(path)) throw new ArgumentNullException("path");
            _directoryInfo = new DirectoryInfo(path);
        }

        public override string ToString()
        {
            return FullPath;
        }

        public string FullPath
        {
            get { return _directoryInfo.FullName; }
        }

        public bool Exists
        {
            get { return _directoryInfo.Exists; }
        }

        public IEnumerable<FileInfo> EnumerateFiles(string searchPattern = "*")
        {
            return _directoryInfo.EnumerateFiles(searchPattern, SearchOption.AllDirectories);
        }

        public void CreateIfNotExists()
        {
            if (_directoryInfo.Exists) return;
            Debug.Print("Creating {0}", _directoryInfo);
            _directoryInfo.Create();
        }

        public void Delete()
        {
            Debug.Print("Deleting {0}", _directoryInfo);
            _directoryInfo.Delete(true);
        }
    }
}