﻿using System;
using System.Linq;

namespace Filemonger
{
    // ReSharper disable MemberCanBePrivate.Global
    public class Statistics
    {
        public int Count;
        public int Sum;
        public double Minimum;
        public double Maximum;
        public double Average;
        public double Range;
        public double Median;
        public double SampleVariance;
        public double SampleStandardDeviation;
        public double PopulationVariance;
        public double PopulationStandardDeviation;

        public Statistics()
        {
        }

        public Statistics(int[] data)
        {
            Calculate(data);
        }

        public void Calculate(int[] data)
        {
            Count = data.Length;
            Sum = data.Sum();
            Minimum = int.MaxValue;
            Maximum = int.MinValue;
            Average = 0.0;
            foreach (var value in data)
            {
                Average += value;
                if (value < Minimum) Minimum = value;
                if (value > Maximum) Maximum = value;
            }
            Average /= Count;
            Range = Maximum - Minimum;

            var v = data.Select(value => value - Average).Select(diff => (diff*diff)).Sum();
            PopulationVariance = v / Count;
            PopulationStandardDeviation = Math.Sqrt(PopulationVariance);

            SampleVariance = v / (Count - 1);
            SampleStandardDeviation = Math.Sqrt(SampleVariance);

            var sorted = data.OrderBy(d => d).ToArray();
            if ((Count & 1) == 0)
            {
                int i = Count/2;
                Median = (sorted[i - 1] + sorted[i]) / 2.0;
            }
            else
            {
                Median = sorted[Count / 2];
            }
        }
    }
    // ReSharper restore MemberCanBePrivate.Global
}