create or replace package Filemonger is

  -- Author  : TLC
  -- Created : 12-03-2012 15:43:33
  -- Purpose : Make full file paths for media files.

  -- Public type declarations
  subtype media_guid is raw(32);

  type media_locator is record(
    guid        media_guid,
    mount_point integer,
    top_folder  integer,
    sub_folder  integer);

  -- Public constant declarations
  mask_mount_point constant raw(4) := '001';
  mask_top_folder  constant raw(4) := '007';
  mask_sub_folder  constant raw(4) := '007';

  -- Public function and procedure declarations

  -- Returns a mediamask based on the constants mask_mount_point, mask_top_folder and mask_sub_folder
  function MediaMask return media_guid;

  -- Calculates a media guid based on the data, the algorithm used is md5
  function GetMediaGuid(data varchar2) return media_guid;

  -- Convert a media_guid to a media_locator using the constants mask_mount_point, mask_top_folder and mask_sub_folder
  function GetMediaLocator(muid media_guid) return media_locator;

  -- Construct a full unc path using the mount point defined in media_mount_points
  function GetMediaPath(muid media_guid) return varchar2;

  -- Construct a full uri using the mount point defined in media_mount_points
  function GetMediaUri(muid media_guid) return varchar2;

  -- Reinitialize table media_mount_points with mount points
  -- Be aware that the table is cleared before it's refilled !!!
  procedure InitMountPoints;

end;
/
create or replace package body Filemonger is

  media_mask media_guid;

  mask_subsize_mount_point binary_integer := length(mask_mount_point);
  mask_subsize_top_folder  binary_integer := length(mask_top_folder);
  mask_subsize_sub_folder  binary_integer := length(mask_sub_folder);

  mask_idx_mount_point binary_integer := 1;
  mask_idx_top_folder  binary_integer := mask_idx_mount_point +
                                         mask_subsize_mount_point;
  mask_idx_sub_folder  binary_integer := mask_idx_top_folder +
                                         mask_subsize_top_folder;

  --mask_ofs binary_integer := 9 - mask_sub_size;

  -- private
  function RawToInt(val raw, pos integer, len integer) return integer is
  begin
    return utl_raw.cast_to_binary_integer(substr(val, pos, len));
  end;

  -- private
  function IntToRaw(val integer, subsize integer) return raw is
  begin
    return substr(utl_raw.cast_from_binary_integer(val), 9 - subsize);
  end;

  -- private
  function IntToPath(val integer) return varchar2 is
  begin
    return lpad(val, 5, '0');
  end;

  -- private
  function GetMediaMask(mount_point raw, top_folder raw, sub_folder raw)
    return media_guid is
  begin
    return utl_raw.concat(IntToRaw(utl_raw.cast_to_binary_integer(mount_point),
                                   mask_subsize_mount_point),
                          IntToRaw(utl_raw.cast_to_binary_integer(top_folder),
                                   mask_subsize_top_folder),
                          IntToRaw(utl_raw.cast_to_binary_integer(sub_folder),
                                   mask_subsize_sub_folder));
  end;

  -- private
  function MakeLocator(muid raw) return media_locator is
    masked_guid media_guid;
    mf          media_locator;
  begin
    masked_guid := utl_raw.bit_and(muid, media_mask);
  
    mf.guid        := muid;
    mf.mount_point := RawToInt(masked_guid,
                               mask_idx_mount_point,
                               mask_subsize_mount_point);
    mf.top_folder  := RawToInt(masked_guid,
                               mask_idx_top_folder,
                               mask_subsize_top_folder);
    mf.sub_folder  := RawToInt(masked_guid,
                               mask_idx_sub_folder,
                               mask_subsize_sub_folder);
    return mf;
  end;

  -- private
  function FormatUri(mount_point raw,
                     top_folder  raw,
                     sub_folder  raw,
                     seperator   char) return varchar2 is
  begin
    return mount_point || seperator || top_folder || seperator || sub_folder || seperator;
  end;

  -- private
  function FormatUri(locator media_locator, seperator char) return varchar2 is
  begin
    return FormatUri(IntToRaw(locator.mount_point,
                              mask_subsize_mount_point),
                     IntToRaw(locator.top_folder, mask_subsize_top_folder),
                     IntToRaw(locator.sub_folder, mask_subsize_sub_folder),
                     seperator);
  end;

  -- public
  function MediaMask return media_guid is
  begin
    return media_mask;
  end;

  -- public
  function GetMediaGuid(data varchar2) return media_guid is
    v_media_guid media_guid;
  begin
    v_media_guid := DBMS_CRYPTO.Hash(utl_raw.cast_to_raw(data),
                                     DBMS_CRYPTO.HASH_MD5);
    return v_media_guid;
  end;

  -- public
  function GetMediaLocator(muid media_guid) return media_locator is
  begin
    return MakeLocator(muid);
  end;

  -- private
  function GetMountPoint(mp integer) return media_mount_points%rowtype is
    mmp media_mount_points%rowtype;
  begin
    select * into mmp from media_mount_points where mount_point = mp;
    return mmp;
  end;

  -- public
  function GetMediaPath(muid media_guid) return varchar2 is
    locator media_locator := MakeLocator(muid);
    mmp     media_mount_points%rowtype;
  begin
    mmp := GetMountPoint(locator.mount_point);
    return '\\' || mmp.host || '\' || mmp.prefix || FormatUri(locator, '\') || muid || '.ext';
  end;

  -- public
  function GetMediaUri(muid media_guid) return varchar2 is
    locator media_locator := MakeLocator(muid);
    mmp     media_mount_points%rowtype;
  begin
    mmp := GetMountPoint(locator.mount_point);
    -- uri : file://somewhere/share/topfolder/subfolder/guid.ext
    return mmp.scheme || '://' || mmp.host || '/' || mmp.prefix || FormatUri(locator,
                                                                             '/') || muid || '.ext';
  end;

  -- public
  procedure InitMountPoints is
    v_scheme media_mount_points.scheme%type := 'file';
    v_host   media_mount_points.host%type := 'devstorage';
    v_prefix media_mount_points.host%type := 'root';
    mp_count binary_integer := utl_raw.cast_to_binary_integer(mask_mount_point);
  begin
    delete from media_mount_points;
    commit;
  
    for v_mount_point in 0 .. mp_count loop
      insert into media_mount_points
        (mount_point, scheme, host, prefix)
      values
        (v_mount_point, v_scheme, v_host, v_prefix);
    
    end loop;
    commit;
  end;

begin
  -- Initialization
  media_mask := GetMediaMask(mask_mount_point,
                             mask_top_folder,
                             mask_sub_folder);
end;
/
