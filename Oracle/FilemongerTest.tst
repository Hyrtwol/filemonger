PL/SQL Developer Test script 3.0
36
-- Created on 12-03-2012 by TLC 
declare
  -- Local variables here
  i            integer;
  loc          Filemonger.media_locator;
  v_src        varchar2(256);
  v_src_raw    raw(256);
  v_media_guid Filemonger.media_guid;

  procedure Dump(locator Filemonger.media_locator) is
  begin
    dbms_output.put_line('data        = ' || v_src);
    dbms_output.put_line('guid        = ' || locator.guid);
    dbms_output.put_line('mount_point = ' || locator.mount_point);
    dbms_output.put_line('top_folder  = ' || locator.top_folder);
    dbms_output.put_line('sub_folder  = ' || locator.sub_folder);
    dbms_output.put_line('path        = ' || Filemonger.GetMediaPath(locator.guid));
    dbms_output.put_line('uri         = ' || Filemonger.GetMediaUri(locator.guid));
  end;

begin
  dbms_output.put_line('mask_mount_point = ' || Filemonger.mask_mount_point);
  dbms_output.put_line('mask_top_folder  = ' || Filemonger.mask_top_folder);
  dbms_output.put_line('mask_sub_folder  = ' || Filemonger.mask_sub_folder);
  dbms_output.put_line('');

  v_src        := dbms_random.string('U', 32);
  v_media_guid := Filemonger.GetMediaGuid(v_src);

  dbms_output.put_line('media_guid       = ' || v_media_guid);
  dbms_output.put_line('media_mask       = ' || Filemonger.MediaMask);
  dbms_output.put_line('');

  loc := Filemonger.GetMediaLocator(v_media_guid);
  Dump(loc);
end;
0
0
