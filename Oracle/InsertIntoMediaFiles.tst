PL/SQL Developer Test script 3.0
18
-- Created on 12-03-2012 by TLC 
begin

  delete from media_files;
  commit;

  for v_lm_id in 10001 .. 30000 loop
    for v_encoding_id in 1 .. 2 loop
      insert into media_files
        (lm_id, encoding_id)
      values
        (v_lm_id, v_encoding_id);
    end loop;
  end loop;

  commit;

end;
0
0
