﻿# Filemonger
 
monger
n.
1. A dealer in a specific commodity. Often used in combination: an ironmonger.
2. A person promoting something undesirable or discreditable. Often used in combination: a scandalmonger; a warmonger.
